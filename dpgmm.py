import numpy as np
import random
import math
import pickle
import os
import shutil
import scipy.stats as ss

# program debug control
verbose = 1

# default dpgmm parameters
alpha = 1.0
beta = 0.1
mu0 = 0
S_scale = 1
nu = 15

# fast program control
omit_variation_cluster_param = 0

# animation parameters
animdir = ""
animon = 0
animidx = 0

# cut-off parameters
CUT_OFF_MAX_ITERATIONS = 3000
CUT_OFF_MAX_MIN_RATIO = 5
CUT_OFF_NO_UPDATE = 100
CUT_OFF_EPSILON = 1e-4
CUT_OFF_NUM_NEIGHBORS = 10

def log_gaussian_likelihood(x, mu, Lambda):
    vector = (x - mu)[np.newaxis, :]
    logp = np.log(np.linalg.det(Lambda)) * 0.5 - np.log(2.0 * np.pi) * (num_dim / 2) - 1.0 / 2.0 * np.dot(np.dot(vector, Lambda), vector.T)
    return logp

class SampleData:
    def __init__(self, idx, x):
        self.x = x
        self.idx = idx
        self.cluster = -1
    def set_cluster(self, c):
        self.cluster = c

class Cluster:
    def __init__(self):
        self.sample_data = {}
        self.num_samples  = 0
        self.most_likely_num_samples = 0
        self.most_likely_mu = {}
        self.most_likely_Lambda = {}
        self.logpostp = None
        self.logval_factorial = 0

    def add(self, data):
        self.sample_data[data] = data
        self.num_samples += 1
        self.update()
        if self.num_samples > 2:
            self.logval_factorial += np.log(self.num_samples - 1)

    def delete(self, data):
        del self.sample_data[data]
        self.num_samples -= 1
        if self.num_samples != 0:
            self.update()
            self.logval_factorial -= np.log(self.num_samples)

    def update(self):
        self.sample_mean = self.calc_mean()
        self.sample_nuc = self.calc_nuc()
        self.sample_Sq = self.calc_Sq()
        if omit_variation_cluster_param == 1:
            self.sample_Lambda = self.sample_nuc * self.sample_Sq
            self.sample_mu = self.calc_muc()
        else:
            self.sample_Lambda = ss.wishart.rvs(df = self.sample_nuc, scale = self.sample_Sq, size=1)
            self.sample_mu = ss.multivariate_normal.rvs(mean=self.calc_muc(), cov=np.linalg.inv(self.calc_Lambdac()), size=1)
        self.logpostp = None

    def calc_mean(self):
        sum = 0
        for data in self.sample_data.values():
            sum += data.x
        return sum / self.num_samples
            
    def calc_nuc(self):
        return nu + self.num_samples

    def calc_Sq(self):
        vector = (self.sample_mean - mu0)[np.newaxis, :]
        Sq_inv = S_inv + self.num_samples * beta / (self.num_samples + beta) * np.dot(vector.T, vector)
        for data in self.sample_data.values():
            vector = (data.x - self.sample_mean)[np.newaxis, :]
            Sq_inv += np.dot(vector.T, vector)
        return np.linalg.inv(Sq_inv)

    def calc_muc(self):
        muc = (self.num_samples * self.sample_mean + beta * mu0) / ( self.num_samples + beta)
        return muc

    def calc_Lambdac(self):
        return (self.num_samples + beta) * self.sample_Lambda

    def get_logpostp(self):
        if self.logpostp is not None:
            return self.logpostp
        else:
            self.logpostp = 0
            if omit_variation_cluster_param == 0:
                self.logpostp += log_gaussian_likelihood(self.sample_mu, mu0, beta * self.sample_Lambda)
                self.logpostp += ss.wishart.logpdf(self.sample_Lambda, nu, S)
            for data in self.sample_data.values():
                self.logpostp += log_gaussian_likelihood(data.x, self.sample_mu, self.sample_Lambda)
            return self.logpostp
            
                
class ClusteringWithNormalWishart:
    def __init__(self, num_dim):
        global S

        random.seed()
        self.S = S = np.identity(num_dim) * S_scale
        print ('S at init = ', S)
        self.nu = nu
        self.clusters = {}
        self.total_samples = 0
        self.samples = {}
        self.most_likely_clusters = {}
        self.most_likely_num_clusters = 0
        self.iters = 0
        self.labels = {}
        self.Sb = {}

    def set_parameters(self, **args):
        global num_dim, alpha, beta, mu0, S, nu, animdir
        if 'num_dim' in args:
            num_dim = args['num_dim']
        if 'alpha' in args:
            alpha = args['alpha']
        if 'beta' in args:
            beta = args['beta']
        if 'mu0' in args:
            mu0 = args['mu0']
        if 'S' in args:
            self.S = S = args['S']
            print ('S at setparam = ', S)
        if 'nu' in args:
            nu = args['nu']
            self.nu = nu
        if 'animdir' in args:
            animdir = args['animdir']
        if 'omit_variation_cluster_param' in args:
            omit_variation_cluster_param = args['omit_variation_cluster_param']
            
    def init_clustering(self, sample_x, b_one_cluster):
        global mu0, S_inv

        S_det = np.linalg.det(S)
        S_inv = np.linalg.inv(S)
        log_gamma1 = math.lgamma((nu + 1.0) / 2.0)
        log_gamma2 = math.lgamma((nu + 1.0 - num_dim) / 2.0)
        self.logval_AF = self.log_AF(alpha, len(sample_x))
        self.logval_part_E_px_new = (np.log(beta) - np.log(1.0 + beta) - np.log(np.pi)) * (num_dim / 2.0) + log_gamma1 - (np.log(S_det) * (nu / 2.0) + log_gamma2)

        self.total_samples = len(sample_x)
        for i in range(len(sample_x)):
            mu0 += sample_x[i]
        mu0 /= self.total_samples
        if b_one_cluster == 0:
            self.num_clusters = len(sample_x)
            for i in range(len(sample_x)):
                c = Cluster()
                d = SampleData(i, sample_x[i])
                d.set_cluster(c)
                c.add(d)
                self.samples[i] = d
                self.clusters[c] = c
        else:
            self.num_clusters = 1
            c = Cluster()
            for i in range(len(sample_x)):
                d = SampleData(i, sample_x[i])
                d.set_cluster(c)
                c.add(d)
                self.samples[i] = d
            self.clusters[c] = c

    def select_cluster(self, one_out_data):
        one_out_x = one_out_data.x
        p = np.zeros(self.num_clusters + 1)
        logp = np.zeros(self.num_clusters + 1)
        tmp_clusters = {}
        i = 0
        for cluster in self.clusters.values():
            logp[i] = np.log(cluster.num_samples) - np.log(self.total_samples - 1 + alpha) + log_gaussian_likelihood(one_out_x, cluster.sample_mu, cluster.sample_Lambda)
            tmp_clusters[i] = cluster
            i += 1
        if one_out_data in self.Sb:
            Sb = self.Sb[one_out_data]
        else:
            vector = (one_out_x - mu0)[np.newaxis, :]
            Sb_inv = S_inv + beta / (1.0 + beta) * np.dot(vector.T, vector)
            Sb = np.linalg.inv(Sb_inv)
        log_E_px_new = self.logval_part_E_px_new + np.log(np.linalg.det(Sb)) * ((nu + 1.0) / 2.0)
        logp[self.num_clusters] = np.log(alpha) - np.log(self.total_samples - 1.0 + alpha) + log_E_px_new
        for i in range(self.num_clusters + 1):
            p[i] = np.exp(logp[i])
            if i != 0:
                p[i] += p[i-1]
        p /= p[self.num_clusters]
        rnd = random.random()
        for i in range(self.num_clusters):
            if p[i] >= rnd:
                return tmp_clusters[i]
        return None

    def log_AF(self, a, n):
        logval = 0
        for i in range(n):
            logval += np.log(a + i)
        return logval
            
    def calc_log_Ewens(self):
        logval = np.log(alpha) * self.num_clusters
        for cluster in self.clusters.values():
            logval += cluster.logval_factorial
        logval -= self.logval_AF
        return logval
            
    def posterior_probability(self):
        logp = self.calc_log_Ewens()
        for cluster in self.clusters.values():
            logp += cluster.get_logpostp()
        #return np.exp(logp)
        return logp

    def copy_most_likely_clusters(self):
        self.most_likely_clusters = {}
        self.labels = np.zeros(self.total_samples, dtype=int)
        i = 0
        for cluster in self.clusters.values():
            c = Cluster()
            for data in cluster.sample_data.values():
                c.add(data)
                self.labels[data.idx] = i
            c.most_likely_num_samples = cluster.num_samples
            c.most_likely_mu = cluster.sample_mu
            c.most_likely_Lambda = cluster.sample_Lambda
            self.most_likely_clusters[i] = c
            i += 1
        self.most_likely_num_clusters = self.num_clusters

    def dump_most_likely_clusters(self, filename):
        f = open(filename, 'wb')
        pickle.dump(self.most_likely_num_clusters, f)
        for i in range(self.most_likely_num_clusters):
            pickle.dump(self.most_likely_clusters[i].most_likely_num_samples, f)
            pickle.dump(self.most_likely_clusters[i].most_likely_mu, f)
            pickle.dump(np.linalg.inv(self.most_likely_clusters[i].most_likely_Lambda), f)
        f.close()
        
    def dump_clusters_param(self, filename):
        f = open(filename, 'wb')
        pickle.dump(self.num_clusters, f)
        for cluster in self.clusters.values():
            pickle.dump(cluster.num_samples, f)
            pickle.dump(cluster.sample_mu, f)
            pickle.dump(np.linalg.inv(cluster.sample_Lambda), f)
        f.close()
        
    def fit(self, sample_x, b_one_cluster, method):
        global animdir, animidx, animon

        self.init_clustering(sample_x, b_one_cluster)
        self.copy_most_likely_clusters()

        if animdir != "":
            animidx = 0
            animon = 1
            try:
                os.makedirs(animdir)
            except:
                shutil.rmtree(animdir)
                os.makedirs(animdir)
            animfile = animdir + '/' + '%06d' % (animidx) + '.pkl'
            animidx += 1
            self.dump_most_likely_clusters(animfile)
            logfile = animdir + '/log.txt'
            logf = open(logfile, 'w')
        
        if method == "cyclically_sequential":
            max_iters = CUT_OFF_MAX_ITERATIONS
            min_no_update_cnt = CUT_OFF_NO_UPDATE
            max_no_update_cnt = CUT_OFF_NO_UPDATE * CUT_OFF_MAX_MIN_RATIO
            thresh_num_neighbors = CUT_OFF_NUM_NEIGHBORS
        elif method == "completely_random":
            max_iters = CUT_OFF_MAX_ITERATIONS * self.total_samples
            min_no_update_cnt = CUT_OFF_NO_UPDATE * self.total_samples
            max_no_update_cnt = CUT_OFF_NO_UPDATE * self.total_samples * CUT_OFF_MAX_MIN_RATIO
            thresh_num_neighbors = CUT_OFF_NUM_NEIGHBORS
        max_post_p = -10000
        no_update_cnt = 0
        num_neighbors = 0
        for i in range(max_iters):
            if method == "cyclically_sequential":
                for one_out_data in self.samples.values():
                    one_out_cluster = one_out_data.cluster
                    one_out_cluster.delete(one_out_data)
                    if one_out_cluster.num_samples == 0:
                        del self.clusters[one_out_cluster]
                        self.num_clusters -= 1
                    new_c = self.select_cluster(one_out_data)
                    if new_c is None:
                        new_c = Cluster()
                        self.num_clusters += 1
                        self.clusters[new_c] = new_c
                    new_c.add(one_out_data)
                    one_out_data.set_cluster(new_c)
                    if animon and k != c_idx:
                        animfile = animdir + '/' + '%06d' % (animidx) + '.pkl'
                        animidx += 1
                        self.dump_clusters_param(animfile)
                one_out_cluster = None
            elif method == "completely_random":
                r = random.randint(0, self.total_samples - 1)
                one_out_data = self.samples[r]
                one_out_cluster = one_out_data.cluster
                one_out_cluster.delete(one_out_data)
                if one_out_cluster.num_samples == 0:
                    del self.clusters[one_out_cluster]
                    self.num_clusters -= 1
                new_c = self.select_cluster(one_out_data)
                if new_c is None:
                    new_c = Cluster()
                    self.num_clusters += 1
                    self.clusters[new_c] = new_c
                new_c.add(one_out_data)
                one_out_data.set_cluster(new_c)
                if animon and one_out_cluster != new_c:
                    animfile = animdir + '/' + '%06d' % (animidx) + '.pkl'
                    animidx += 1
                    self.dump_clusters_param(animfile)
            if omit_variation_cluster_param == 0 or one_out_cluster != new_c:
                post_p = self.posterior_probability()
                if post_p > max_post_p and abs(post_p - max_post_p) > 1.0e-10:
                    if verbose == 1:
                        print ('post_p - max_post_p = ', post_p - max_post_p)
                        print ('post_p = ', post_p, ', max_post_p = ', max_post_p, ', i = ', i, ', num_clusters = ', self.num_clusters)
                    if animon:
                        print ('idx = ', animidx, ', num_clusters = ', self.num_clusters, file=logf)
                        animfile = animdir + '/' + '%06d' % (animidx) + '.pkl'
                        animidx += 1
                        self.dump_clusters_param(animfile)
                    max_post_p = post_p
                    self.copy_most_likely_clusters()
                    no_update_cnt = 0
                    num_neighbors = 0
                else:
                    no_update_cnt += 1
                    if abs(np.exp(max_post_p) - np.exp(post_p)) <= CUT_OFF_EPSILON:
                        num_neighbors += 1
            else:
                no_update_cnt += 1
            if (no_update_cnt >= max_no_update_cnt) or \
               (no_update_cnt >= min_no_update_cnt and num_neighbors >= thresh_num_neighbors):
                self.iters = i
                if verbose == 1:
                    print ('no_update_cnt = ', no_update_cnt, ', num_neighbors = ', num_neighbors)
                if animon:
                    logf.close()
                return
        print ('max iterations')
        self.iters = i
        if animon:
            logf.close()
