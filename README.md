**実行方法**  
python test_dpgmm.py

**test_dpgmm.py 内の変更可能パラメータ**

クラスタリングパラメータ  
- alpha  
- beta  
- mu0  
- S  
- nu  

動作制御パラメータ  
- test_pattern           [サンプルのパターン指定]  
- start_with_one_cluster [1つのクラスタからスタートするかどうか]  
- algo_method            [一つ抜きの方法]  
- animdir                [指定したディレクトリに pkl ファイルを作る]  
- generate_sample        [新しくサンプルを作成するか以前作成したサンプルを再利用するか]  
