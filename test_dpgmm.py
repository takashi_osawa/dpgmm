import numpy as np
import pylab as plt
import sys
import os
import shutil
import pickle
import time
import dpgmm

# program debug control
verbose = 1

# default dpgmm parameters
num_dim = 2
alpha = 1.0
beta = 0.1
mu0 = 0
S = np.identity(2)*1.0
nu = 15

# fast program control
omit_variation_cluster_param = 0

# program control parameters
test_pattern = 1
start_with_one_cluster = 0
algo_method = "completely_random"
#algo_method = "cyclically_sequential"
#animdir = "z:/ohsawa/fixstars/blog/anim016/pkl"
animdir = ""
generate_sample = 1

if test_pattern == 0:
    one_cluster_sample = 1
    sample_Sigma = [[0.25, 0], [0, 0.25]]
else:
    one_cluster_sample = 0
    sample_Sigma = {}
    if test_pattern == 1:
        for i in range(5):
            sample_Sigma[i] = [[0.05, 0], [0, 0.05]]
    elif test_pattern == 2:
        for i in range(5):
            sample_Sigma[i] = [[0.0001, 0], [0, 0.0001]]
    elif test_pattern == 3:
        sample_Sigma[0] = [[0.03, 0], [0, 0.03]]
        sample_Sigma[1] = [[0.05, 0], [0, 0.05]]
        sample_Sigma[2] = [[0.05, 0], [0, 0.003]]
        sample_Sigma[3] = [[0.01, 0], [0, 0.01]]
        sample_Sigma[4] = [[0.04, 0.0], [0.0, 0.04]]

sample_mean = {}
sample_mean[0] = [0.5, 0.5]
sample_mean[1] = [2.5, 0.5]
sample_mean[2] = [0.5, 2.5]
sample_mean[3] = [1.8, 1.8]
sample_mean[4] = [2.5, 2.5]

def main(args):
    if test_pattern == 0:
        sample_file = 'sample4_0.pkl'
    elif test_pattern == 1:
        sample_file = 'sample4_1.pkl'
    elif test_pattern == 2:
        sample_file = 'sample4_2.pkl'
    elif test_pattern == 3:
        sample_file = 'sample4_3.pkl'

    if generate_sample == 1:
        if one_cluster_sample == 0:
            a = np.random.multivariate_normal(sample_mean[0], sample_Sigma[0], 10)
            b = np.random.multivariate_normal(sample_mean[1], sample_Sigma[1], 10)
            c = np.random.multivariate_normal(sample_mean[2], sample_Sigma[2], 10)
            d = np.random.multivariate_normal(sample_mean[3], sample_Sigma[3], 10)
            e = np.random.multivariate_normal(sample_mean[4], sample_Sigma[4], 10)
        else:
            a = np.random.multivariate_normal([1.5, 1.5], sample_Sigma, 100)
            b = np.random.multivariate_normal([1.5, 1.5], sample_Sigma, 0)
            c = np.random.multivariate_normal([1.5, 1.5], sample_Sigma, 0)
            d = np.random.multivariate_normal([1.5, 1.5], sample_Sigma, 0)
            e = np.random.multivariate_normal([1.5, 1.5], sample_Sigma, 0)
        f = open(sample_file, 'wb')
        pickle.dump(a, f)
        pickle.dump(b, f)
        pickle.dump(c, f)
        pickle.dump(d, f)
        pickle.dump(e, f)
        f.close()
    else:
        f = open(sample_file, 'rb')
        a = pickle.load(f)
        b = pickle.load(f)
        c = pickle.load(f)
        d = pickle.load(f)
        e = pickle.load(f)
        f.close()
    X = np.append(a, b, axis=0)
    X = np.append(X, c, axis=0)
    X = np.append(X, d, axis=0)
    X = np.append(X, e, axis=0)
    plt.clf()
    plt.plot(a.T[0], a.T[1], 'o')
    plt.plot(b.T[0], b.T[1], 'o')
    plt.plot(c.T[0], c.T[1], 'o')
    plt.plot(d.T[0], d.T[1], 'o')
    plt.plot(e.T[0], e.T[1], 'o')
    plt.show()
    start = time.time()
    clustering = dpgmm.ClusteringWithNormalWishart(num_dim)
    clustering.set_parameters(num_dim=num_dim, alpha=alpha, beta=beta, S=S, nu=nu, animdir=animdir, omit_variation_cluster_param=omit_variation_cluster_param)
    if algo_method == "completely_random":
        clustering.fit(X, start_with_one_cluster, "completely_random")
    else:
        clustering.fit(X, start_with_one_cluster, "cyclically_sequential")
    elapsed_time = time.time() - start
    clustering.dump_most_likely_clusters('cluster4.pkl')
    if verbose == 1:
        print ('num_clusters = ', clustering.most_likely_num_clusters)
        for i in range(clustering.most_likely_num_clusters):
            print ('cluster ', i)
            print (' mu    = ', clustering.most_likely_clusters[i].most_likely_mu)
            print (' Lamda = ', clustering.most_likely_clusters[i].most_likely_Lambda)
            print (' Sigma = ', np.linalg.inv(clustering.most_likely_clusters[i].most_likely_Lambda))
    f = open('expance4.pkl', 'wb')
    pickle.dump(clustering.nu, f)
    pickle.dump(clustering.S, f)
    f.close()
    print ("elapsed_time:{0}".format(elapsed_time) + "[sec]")
    if verbose == 1:
        print ('iters = ', clustering.iters)
    for (x, y), c in zip(X, clustering.labels):
#        plt.plot(x, y, "o^v<>s*xDd1234hHp"[c], color="RGBYKMCRGBYKMC"[c])
        plt.plot(x, y, 'o', color="RGBYKMCRGBYKMC"[c])
    plt.show()

if __name__ == "__main__":
    main(sys.argv)
